from kafka_message_system.configs import ConsumerConfig, ProducerConfig, KafkaConfig
from kafka_message_system.messenger import KafkaMessageSystem
import json
import datetime as dd
import traceback

PRODUCE_TOPIC = ''
ERROR_TOPIC = ''


def get_json(path):
    with open(path, 'r') as f:
        return json.loads(''.join(f.readlines()))


def init_kafka(path_to_config_file_consumer, path_to_config_file_producer):
    consumer_config_json = get_json(path_to_config_file_consumer)
    consumer_config = ConsumerConfig(consumer_config_json['topic'],
                                     consumer_config_json['server'],
                                     lambda x: json.loads(x.decode('utf-8')),
                                     auto_offset_reset=consumer_config_json['auto_offset_reset'])
    producer_config_json = get_json(path_to_config_file_producer)
    producer_config = ProducerConfig(producer_config_json['server'], lambda x: json.dumps(x).encode('utf-8'))
    global PRODUCE_TOPIC, ERROR_TOPIC
    PRODUCE_TOPIC = producer_config_json['produce-topic']
    ERROR_TOPIC = producer_config_json['error-topic']
    return KafkaMessageSystem(KafkaConfig(producer_config, consumer_config))


def fix_types(data):
    if type(data) is list:
        result = []
        for d in data:
            result.append(fix_types(d))
        data = result
    elif type(data) is dict:
        result = {}
        for key in data.keys():
            result[key] = fix_types(data[key])
        data = result
    elif isinstance(data, dd.datetime):
        return data.isoformat()
    return data


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def process_events(messaging_system: KafkaMessageSystem):
    topic_partitions = messaging_system.consume_message()
    for topic_partition_key in topic_partitions.keys():
        for message in topic_partitions[topic_partition_key]:
            nested_dict = fix_types(message.value)
            flatten = flatten_json(nested_dict)
            flatten['RawDataSriService'] = str(message.value)
            flatten['AddedToElasticDate'] = dd.datetime.now().isoformat()
            print(flatten)
            
            messaging_system.produce_message(PRODUCE_TOPIC, flatten)
            print('message sent')
        messaging_system.flush_messages()


if __name__ == '__main__':
    kafka = init_kafka('./configs/consumer.json', './configs/producer.json')
    while True:
        try:
            process_events(kafka)
        except Exception as e:
            logs_data = {'Error': str(e),
                         'Trace': "".join(traceback.TracebackException.from_exception(e).format())}
            kafka.produce_message(ERROR_TOPIC, logs_data)
